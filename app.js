var express = require('express')
var bodyParser = require('body-parser')
var api = require('./routes/api')
var app = express()
var port = 3000
var dadosEggs = require('./data/mockedData')
var dadosLogin = require('./Login')


app.use(bodyParser())

app.get('/api/v1/easter-eggs', (req, res) => {
  //res.send(JSON.stringify(dadosEggs))
  res.json(dadosEggs)
});

//Rotas

app.get('/api/v1/easter-eggs/:egg-id', (req, res) =>{
  dadosEggs.find(req.params.id, (dados) => {
    res.send('<h1>Teste</h1>')
    res.json(dados)
  })

})

/* Middleware*/
app.use(function(req, res, next) {
  console.log('Atualizando...');
  next();
});


app.get('/', function(req, res) {
  res.json({ message: 'Seja Bem-Vindo a API' });
});


/*
app.get('/usuario',(req, res) => {
  res.send(`
    <h1>Usuario</h1>
    <form action="/usuario" method="POST">
      <label for="login">Login: </label>
      <input type="login" name="login" id="login">
      <label for="password">Password: </label>
      <input type="password" name="password" id="password">
      <input type="submit" value="Enviar">
    </form>
  `)
})

app.post('/usuario', (req, res) => {
  res.send('<h1>Teste</h1>')
})*/

app.use(api);

app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}.`)
})